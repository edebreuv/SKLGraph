<main>
<article id="content">
<header>
<h1 class="title">Module <code>skl_fgraph</code></h1>
</header>
<section id="section-intro">
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python"># Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# skl_fgraph=Skeleton graph with computable features; Derived from base skeleton graph.

from typing import Callable, Iterable, List, Optional, Tuple  # , SupportsFloat

import numpy as np_

from skl_graph import skl_graph_t as skl_nfgraph_t  # nf=no feature

class skl_graph_t(skl_nfgraph_t):
    #
    @property
    def highest_degree(self) -> int:
        return max(degree for ___, degree in self.degree)

    @property
    def highest_degree_w_nodes(self) -> Tuple[int, List[str]]:
        #
        max_degree = -1
        at_nodes = None
        for node, degree in self.degree:
            if degree > max_degree:
                max_degree = degree
                at_nodes = [node]
            elif degree == max_degree:
                at_nodes.append(node)

        return max_degree, at_nodes

    @property
    def edge_lengths(self) -> Tuple[float, ...]:
        #
        return tuple(
            edge.lengths.length for ___, ___, edge in self.edges.data("as_edge_t")
        )

    @property
    def length(self) -> float:
        #
        return sum(self.edge_lengths)

    @property
    def area_as_rw_x_l(self) -> Optional[float]:
        """Area as reduced width times length.

        Returns
        -------

        """
        if self.has_widths:
            return self.ReducedWidth() * self.length
        else:
            return None

    @property
    def edge_ww_lengths(self) -> Tuple[float, ...]:
        #
        return tuple(
            edge.lengths.ww_length for ___, ___, edge in self.edges.data("as_edge_t")
        )

    @property
    def ww_length(self) -> float:
        #
        return sum(self.edge_ww_lengths)

    def EdgeReducedWidths(
        self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
    ) -> Tuple[float, ...]:
        #
        return tuple(
            reduce_fct(edge.widths) for ___, ___, edge in self.edges.data("as_edge_t")
        )

    def ReducedWidth(
        self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
    ) -> float:
        #
        all_widths = []
        for ___, ___, edge in self.edges.data("as_edge_t"):
            all_widths.extend(edge.widths)

        return reduce_fct(all_widths)

    def HeterogeneousReducedWidth(
        self,
        edge_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
        final_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
    ) -> float:
        #
        return final_reduce_fct(self.EdgeReducedWidths(edge_reduce_fct))

    def __str__(self) -> str:
        """"""
        output = super().__str__()
        output += (
            f"\n"
            f"    Highest degree={self.highest_degree}\n\n"
            f"    Length={round(self.length, 2)}"
        )
        if self.has_widths:
            output += (
                f"\n"
                f"    Width: Hom={round(self.ReducedWidth(), 2)}, "
                f"Het={round(self.HeterogeneousReducedWidth(), 2)}\n"
                f"    Area: RWxL={round(self.area_as_rw_x_l, 2)}, "
                f"WWL={round(self.ww_length, 2)}"
            )

        return output</code></pre>
</details>
</section>
<section>
</section>
<section>
</section>
<section>
</section>
<section>
<h2 class="section-title" id="header-classes">Classes</h2>
<dl>
<dt id="skl_fgraph.skl_graph_t"><code class="flex name class">
<span>class <span class="ident">skl_graph_t</span></span>
</code></dt>
<dd>
<div class="desc"><p>s_node: Singleton node
e_node: End node
b_node: Branch node</p>
<p>Initialize a graph with edges, name, or graph attributes.</p>
<h2 id="parameters">Parameters</h2>
<dl>
<dt><strong><code>incoming_graph_data</code></strong> : <code>input graph</code></dt>
<dd>Data to initialize graph.
If incoming_graph_data=None (default)
an empty graph is created.
The data can be an edge list, or any
NetworkX graph object.
If the corresponding optional Python
packages are installed the data can also be a NumPy matrix
or 2d ndarray, a SciPy sparse matrix, or a PyGraphviz graph.</dd>
<dt><strong><code>attr</code></strong> : <code>keyword arguments</code>, optional <code>(default= no attributes)</code></dt>
<dd>Attributes to add to graph as key=value pairs.</dd>
</dl>
<h2 id="see-also">See Also</h2>
<p><code>convert</code></p>
<h2 id="examples">Examples</h2>
<pre><code class="language-python-repl">>>> G = nx.Graph()  # or DiGraph, MultiGraph, MultiDiGraph, etc
>>> G = nx.Graph(name="my graph")
>>> e = [(1, 2), (2, 3), (3, 4)]  # list of edges
>>> G = nx.Graph(e)
</code></pre>
<p>Arbitrary graph attribute pairs (key=value) may be assigned</p>
<pre><code class="language-python-repl">>>> G = nx.Graph(e, day="Friday")
>>> G.graph
{'day': 'Friday'}
</code></pre></div>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">class skl_graph_t(skl_nfgraph_t):
    #
    @property
    def highest_degree(self) -> int:
        return max(degree for ___, degree in self.degree)

    @property
    def highest_degree_w_nodes(self) -> Tuple[int, List[str]]:
        #
        max_degree = -1
        at_nodes = None
        for node, degree in self.degree:
            if degree > max_degree:
                max_degree = degree
                at_nodes = [node]
            elif degree == max_degree:
                at_nodes.append(node)

        return max_degree, at_nodes

    @property
    def edge_lengths(self) -> Tuple[float, ...]:
        #
        return tuple(
            edge.lengths.length for ___, ___, edge in self.edges.data("as_edge_t")
        )

    @property
    def length(self) -> float:
        #
        return sum(self.edge_lengths)

    @property
    def area_as_rw_x_l(self) -> Optional[float]:
        """Area as reduced width times length.

        Returns
        -------

        """
        if self.has_widths:
            return self.ReducedWidth() * self.length
        else:
            return None

    @property
    def edge_ww_lengths(self) -> Tuple[float, ...]:
        #
        return tuple(
            edge.lengths.ww_length for ___, ___, edge in self.edges.data("as_edge_t")
        )

    @property
    def ww_length(self) -> float:
        #
        return sum(self.edge_ww_lengths)

    def EdgeReducedWidths(
        self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
    ) -> Tuple[float, ...]:
        #
        return tuple(
            reduce_fct(edge.widths) for ___, ___, edge in self.edges.data("as_edge_t")
        )

    def ReducedWidth(
        self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
    ) -> float:
        #
        all_widths = []
        for ___, ___, edge in self.edges.data("as_edge_t"):
            all_widths.extend(edge.widths)

        return reduce_fct(all_widths)

    def HeterogeneousReducedWidth(
        self,
        edge_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
        final_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
    ) -> float:
        #
        return final_reduce_fct(self.EdgeReducedWidths(edge_reduce_fct))

    def __str__(self) -> str:
        """"""
        output = super().__str__()
        output += (
            f"\n"
            f"    Highest degree={self.highest_degree}\n\n"
            f"    Length={round(self.length, 2)}"
        )
        if self.has_widths:
            output += (
                f"\n"
                f"    Width: Hom={round(self.ReducedWidth(), 2)}, "
                f"Het={round(self.HeterogeneousReducedWidth(), 2)}\n"
                f"    Area: RWxL={round(self.area_as_rw_x_l, 2)}, "
                f"WWL={round(self.ww_length, 2)}"
            )

        return output</code></pre>
</details>
<h3>Ancestors</h3>
<ul class="hlist"><li><a title="skl_graph.skl_graph_t" href="skl_graph.html#skl_graph.skl_graph_t">skl_graph_t</a></li><li>networkx.classes.multigraph.MultiGraph</li><li>networkx.classes.graph.Graph</li></ul><h3>Class variables</h3>
<dl>
<dt id="skl_fgraph.skl_graph_t.colormap"><code class="name">var <span class="ident">colormap</span> : ClassVar[Dict[int, str]]</code></dt>
<dd>
<div class="desc"/>
</dd>
<dt id="skl_fgraph.skl_graph_t.font_size"><code class="name">var <span class="ident">font_size</span> : ClassVar[float]</code></dt>
<dd>
<div class="desc"/>
</dd>
<dt id="skl_fgraph.skl_graph_t.width"><code class="name">var <span class="ident">width</span> : ClassVar[float]</code></dt>
<dd>
<div class="desc"/>
</dd>
</dl>
<h3>Instance variables</h3>
<dl>
<dt id="skl_fgraph.skl_graph_t.area_as_rw_x_l"><code class="name">var <span class="ident">area_as_rw_x_l</span> : Union[float, NoneType]</code></dt>
<dd>
<div class="desc"><p>Area as reduced width times length.</p>
<h2 id="returns">Returns</h2></div>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def area_as_rw_x_l(self) -> Optional[float]:
    """Area as reduced width times length.

    Returns
    -------

    """
    if self.has_widths:
        return self.ReducedWidth() * self.length
    else:
        return None</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.edge_lengths"><code class="name">var <span class="ident">edge_lengths</span> : Tuple[float, ...]</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def edge_lengths(self) -> Tuple[float, ...]:
    #
    return tuple(
        edge.lengths.length for ___, ___, edge in self.edges.data("as_edge_t")
    )</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.edge_ww_lengths"><code class="name">var <span class="ident">edge_ww_lengths</span> : Tuple[float, ...]</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def edge_ww_lengths(self) -> Tuple[float, ...]:
    #
    return tuple(
        edge.lengths.ww_length for ___, ___, edge in self.edges.data("as_edge_t")
    )</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.highest_degree"><code class="name">var <span class="ident">highest_degree</span> : int</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def highest_degree(self) -> int:
    return max(degree for ___, degree in self.degree)</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.highest_degree_w_nodes"><code class="name">var <span class="ident">highest_degree_w_nodes</span> : Tuple[int, List[str]]</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def highest_degree_w_nodes(self) -> Tuple[int, List[str]]:
    #
    max_degree = -1
    at_nodes = None
    for node, degree in self.degree:
        if degree > max_degree:
            max_degree = degree
            at_nodes = [node]
        elif degree == max_degree:
            at_nodes.append(node)

    return max_degree, at_nodes</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.length"><code class="name">var <span class="ident">length</span> : float</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def length(self) -> float:
    #
    return sum(self.edge_lengths)</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.ww_length"><code class="name">var <span class="ident">ww_length</span> : float</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">@property
def ww_length(self) -> float:
    #
    return sum(self.edge_ww_lengths)</code></pre>
</details>
</dd>
</dl>
<h3>Methods</h3>
<dl>
<dt id="skl_fgraph.skl_graph_t.EdgeReducedWidths"><code class="name flex">
<span>def <span class="ident">EdgeReducedWidths</span></span>(<span>self, reduce_fct: Callable[[Iterable[float]], float] = &lt;function mean>) ‑> Tuple[float, ...]</span>
</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">def EdgeReducedWidths(
    self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
) -> Tuple[float, ...]:
    #
    return tuple(
        reduce_fct(edge.widths) for ___, ___, edge in self.edges.data("as_edge_t")
    )</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.HeterogeneousReducedWidth"><code class="name flex">
<span>def <span class="ident">HeterogeneousReducedWidth</span></span>(<span>self, edge_reduce_fct: Callable[[Iterable[float]], float] = &lt;function mean>, final_reduce_fct: Callable[[Iterable[float]], float] = &lt;function mean>) ‑> float</span>
</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">def HeterogeneousReducedWidth(
    self,
    edge_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
    final_reduce_fct: Callable[[Iterable[float]], float] = np_.mean,
) -> float:
    #
    return final_reduce_fct(self.EdgeReducedWidths(edge_reduce_fct))</code></pre>
</details>
</dd>
<dt id="skl_fgraph.skl_graph_t.ReducedWidth"><code class="name flex">
<span>def <span class="ident">ReducedWidth</span></span>(<span>self, reduce_fct: Callable[[Iterable[float]], float] = &lt;function mean>) ‑> float</span>
</code></dt>
<dd>
<div class="desc"/>
<details class="source">
<summary>
<span>Expand source code</span>
</summary>
<pre><code class="python">def ReducedWidth(
    self, reduce_fct: Callable[[Iterable[float]], float] = np_.mean
) -> float:
    #
    all_widths = []
    for ___, ___, edge in self.edges.data("as_edge_t"):
        all_widths.extend(edge.widths)

    return reduce_fct(all_widths)</code></pre>
</details>
</dd>
</dl>
<h3>Inherited members</h3>
<ul class="hlist"><li><code><b><a title="skl_graph.skl_graph_t" href="skl_graph.html#skl_graph.skl_graph_t">skl_graph_t</a></b></code>:
<ul class="hlist"><li><code><a title="skl_graph.skl_graph_t.FromSKLMap" href="skl_graph.html#skl_graph.skl_graph_t.FromSKLMap">FromSKLMap</a></code></li><li><code><a title="skl_graph.skl_graph_t.dim" href="skl_graph.html#skl_graph.skl_graph_t.dim">dim</a></code></li><li><code><a title="skl_graph.skl_graph_t.domain_lengths" href="skl_graph.html#skl_graph.skl_graph_t.domain_lengths">domain_lengths</a></code></li><li><code><a title="skl_graph.skl_graph_t.has_widths" href="skl_graph.html#skl_graph.skl_graph_t.has_widths">has_widths</a></code></li><li><code><a title="skl_graph.skl_graph_t.invalidities" href="skl_graph.html#skl_graph.skl_graph_t.invalidities">invalidities</a></code></li><li><code><a title="skl_graph.skl_graph_t.n_b_nodes" href="skl_graph.html#skl_graph.skl_graph_t.n_b_nodes">n_b_nodes</a></code></li><li><code><a title="skl_graph.skl_graph_t.n_components" href="skl_graph.html#skl_graph.skl_graph_t.n_components">n_components</a></code></li><li><code><a title="skl_graph.skl_graph_t.n_e_nodes" href="skl_graph.html#skl_graph.skl_graph_t.n_e_nodes">n_e_nodes</a></code></li><li><code><a title="skl_graph.skl_graph_t.n_s_nodes" href="skl_graph.html#skl_graph.skl_graph_t.n_s_nodes">n_s_nodes</a></code></li></ul></li></ul></dd>
</dl>
</section>
</article>
<nav id="sidebar">
<h1>Index</h1>
<div class="toc"><ul/></div>
<ul id="index"><li><h3><a href="#header-classes">Classes</a></h3><ul><li><h4><code><a title="skl_fgraph.skl_graph_t" href="#skl_fgraph.skl_graph_t">skl_graph_t</a></code></h4>
<ul class=""><li><code><a title="skl_fgraph.skl_graph_t.EdgeReducedWidths" href="#skl_fgraph.skl_graph_t.EdgeReducedWidths">EdgeReducedWidths</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.HeterogeneousReducedWidth" href="#skl_fgraph.skl_graph_t.HeterogeneousReducedWidth">HeterogeneousReducedWidth</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.ReducedWidth" href="#skl_fgraph.skl_graph_t.ReducedWidth">ReducedWidth</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.area_as_rw_x_l" href="#skl_fgraph.skl_graph_t.area_as_rw_x_l">area_as_rw_x_l</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.colormap" href="#skl_fgraph.skl_graph_t.colormap">colormap</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.edge_lengths" href="#skl_fgraph.skl_graph_t.edge_lengths">edge_lengths</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.edge_ww_lengths" href="#skl_fgraph.skl_graph_t.edge_ww_lengths">edge_ww_lengths</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.font_size" href="#skl_fgraph.skl_graph_t.font_size">font_size</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.highest_degree" href="#skl_fgraph.skl_graph_t.highest_degree">highest_degree</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.highest_degree_w_nodes" href="#skl_fgraph.skl_graph_t.highest_degree_w_nodes">highest_degree_w_nodes</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.length" href="#skl_fgraph.skl_graph_t.length">length</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.width" href="#skl_fgraph.skl_graph_t.width">width</a></code></li><li><code><a title="skl_fgraph.skl_graph_t.ww_length" href="#skl_fgraph.skl_graph_t.ww_length">ww_length</a></code></li></ul></li></ul></li></ul></nav>
</main>
<footer id="footer">
<p>Generated by <a href="https://pdoc3.github.io/pdoc"><cite>pdoc</cite> 0.9.1</a>.</p>
</footer>
