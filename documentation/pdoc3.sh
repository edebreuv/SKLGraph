#!/usr/bin/env sh
BASE_FOLDER="$HOME/Code/brick/stu/skl-graph"
pdoc3 --html --force \
    --output-dir "$BASE_FOLDER/documentation/html/pdoc3" \
    "$BASE_FOLDER/skl_map.py"             \
    "$BASE_FOLDER/skl_graph.py"           \
    "$BASE_FOLDER/skl_fgraph.py"          \
    "$BASE_FOLDER/brick/topology_map.py"  \
    "$BASE_FOLDER/brick/node.py"          \
    "$BASE_FOLDER/brick/edge.py"          \
    "$BASE_FOLDER/brick/elm_id.py"        \
    "$BASE_FOLDER/brick/hierarchy.py"
for html in $(/bin/ls "$BASE_FOLDER/documentation/html/pdoc3/"*.html)
do
    new=$(echo "$html" | sed -n -e 's:/html/:/md/:' -e 's/\.html/.md/p')
    html2md "$html" > "$new"
done
