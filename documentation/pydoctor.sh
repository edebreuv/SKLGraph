#!/usr/bin/env sh
BASE_FOLDER="$HOME/Code/brick/stu/skl-graph"
pydoctor \
    --project-name=SKLGraph \
    --project-url='https://gitlab.inria.fr/edebreuv/SKLGraph' \
    --project-base-dir="$BASE_FOLDER" \
    --add-package="$BASE_FOLDER/skl_map.py" \
    --add-package="$BASE_FOLDER/skl_graph.py" \
    --add-package="$BASE_FOLDER/skl_fgraph.py" \
    --add-package="$BASE_FOLDER/brick/topology_map.py" \
    --add-package="$BASE_FOLDER/brick/node.py" \
    --add-package="$BASE_FOLDER/brick/edge.py" \
    --add-package="$BASE_FOLDER/brick/elm_id.py" \
    --add-package="$BASE_FOLDER/brick/hierarchy.py" \
    --html-output="$BASE_FOLDER/documentation/html/pydoctor"
