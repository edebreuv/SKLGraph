# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from typing import Sequence

import networkx as ntkx
import numpy as nmpy

import skl_graph.type.topology_map as tgmp
from skl_graph.graph import skl_graph_t
from skl_graph.task.check.edge import Invalidities as EdgeInvalidities
from skl_graph.task.check.node import Invalidities as NodeInvalidities


array_t = nmpy.ndarray


def Invalidities(graph: skl_graph_t, /) -> Sequence[str]:
    """"""
    output = []

    n_components = ntkx.number_connected_components(graph)
    if n_components != graph.n_components:
        output.append(
            f"Actual and stored number of connected components differ: "
            f"{n_components}!={graph.n_components}"
        )
    n_s_nodes = sum(degree == 0 for _, degree in graph.degree)
    if n_s_nodes != graph.n_s_nodes:
        output.append(
            f"Actual and stored number of singleton nodes differ: "
            f"{n_s_nodes}!={graph.n_s_nodes}"
        )
    n_e_nodes = sum(degree == 1 for _, degree in graph.degree)
    if n_e_nodes != graph.n_e_nodes:
        output.append(
            f"Actual and stored number of end nodes differ: "
            f"{n_e_nodes}!={graph.n_e_nodes}"
        )
    n_b_nodes = sum(degree > 1 for _, degree in graph.degree)
    if n_b_nodes != graph.n_b_nodes:
        output.append(
            f"Actual and stored number of end nodes differ: "
            f"{n_b_nodes}!={graph.n_b_nodes}"
        )

    for _, details in graph.nodes.data("details"):
        output.extend(NodeInvalidities(details))

    for source, target, details in graph.edges.data("details"):
        source, target = (graph.nodes[_uid]["details"] for _uid in (source, target))
        output.extend(EdgeInvalidities(details, source, target))

    return output


def Correctness(graph: skl_graph_t, skl_map: array_t, /) -> tuple[bool, bool]:
    """"""
    rebuilt_skl_map = graph.RebuiltSkeletonMap()

    topological_map = tgmp.TopologyMapOfMap(skl_map)
    # Keep next line before its next one
    topological_map[topological_map == tgmp.TMapBackgroundLabel(skl_map)] = 0
    topological_map[topological_map > 3] = 3

    binary_correctness = nmpy.array_equal(rebuilt_skl_map > 0, skl_map)
    topology_correctness = nmpy.array_equal(rebuilt_skl_map, topological_map)

    return binary_correctness, topology_correctness
