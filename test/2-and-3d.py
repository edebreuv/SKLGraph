# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import tempfile as tmpf
from pathlib import Path as path_t

import matplotlib.pyplot as pypl
import networkx as ntkx
import numpy as nmpy
import scipy.ndimage as spim
import skimage.measure as sims

from skl_graph.graph import skl_graph_t
from skl_graph.map import CheckSkeletonMap, SKLMapFromObjectMap
from skl_graph.task.check.graph import Correctness, Invalidities
from skl_graph.task.plot.graph import (
    EdgeLabelsForPlot,
    NodeColors,
    NodePositionsForPlot,
    Plot,
    PlotNetworkXGraph,
)
from skl_graph.type.plot import plot_mode_e


array_t = nmpy.ndarray


COLORMAP_FOR_IMG = "inferno"
COLORMAP_FOR_DIFF = "gray"

USE_SKNW = False


if USE_SKNW:
    try:
        import sknw
    except ModuleNotFoundError:
        sknw = None
else:
    sknw = None


def _HorseData() -> tuple[int, array_t, str]:
    """"""
    import skimage.data as sidt
    import skimage.morphology as mplg
    import skimage.util as sitl

    space_dim = 2

    object_map = sitl.invert(sidt.horse())
    disk = mplg.disk(9)
    object_map[10 : (10 + disk.shape[0]), 20 : (20 + disk.shape[1])][disk] = True

    return space_dim, object_map, "multi"


def _RandomData(dim_as_str: str, /) -> tuple[int, array_t, str]:
    """"""
    _SetRandomSeedForSession()

    if dim_as_str == "2d":
        space_dim = 2
        array_shape = (100, 100)
        threshold_fct = lambda img: nmpy.median(img)
        struct_elm_shape = (3, 3)
    elif dim_as_str == "3d":
        space_dim = 3
        array_shape = (80, 80, 50)
        threshold_fct = lambda img: 0.75 * nmpy.amax(image)
        struct_elm_shape = (3, 3, 3)
    else:
        raise ValueError(f"{dim_as_str}: Unknown data specification")

    image = nmpy.random.random(array_shape)
    image = spim.gaussian_filter(image, 1)

    multi_map = image > threshold_fct(image)
    lbl_multi_map, _ = spim.label(
        multi_map,
        structure=nmpy.ones(struct_elm_shape, dtype=nmpy.uint8),
        output=nmpy.uint,
    )

    cc_props = sims.regionprops(lbl_multi_map)
    largest_cc_idx = nmpy.argmax([cc_prop.area for cc_prop in cc_props]).item()
    lbl_multi_map[lbl_multi_map != cc_props[largest_cc_idx].label] = 0
    object_map = lbl_multi_map > 0

    return space_dim, object_map, "single"


def _SetRandomSeedForSession() -> None:
    """Sets a random seed limited to current session.

    The purpose is to allow the generation of different random data in consecutive tests while allowing to generate the
    same data sequence again for debugging purposes. This would be done by deleting the file "skl_graph_demo_seed.lock"
    in the temporary folder.

    Returns
    -------

    """
    try:
        with tmpf.NamedTemporaryFile() as doc_accessor:
            doc_name = path_t(doc_accessor.name)
            tmp_folder = doc_name.parents[0]
    except:
        print("Unable to set a random seed for current session")
        tmp_folder = None

    if tmp_folder is not None:
        seed_lock = tmp_folder.joinpath("skl_graph_demo_seed.lock")
        if not seed_lock.exists():
            nmpy.random.seed(0)
            seed_lock.touch()


def _ObjectMap(data: str, /) -> tuple[int, array_t, str]:
    """"""
    if data == "horse":
        space_dim, object_map, skl_validity_mode = _HorseData()
    elif data in ("2d", "3d"):
        space_dim, object_map, skl_validity_mode = _RandomData(data)
    elif path_t(data).is_file():
        object_map = pypl.imread(data)
        if (object_map.ndim == 3) and (object_map.shape[2] in (3, 4)):
            print(
                f"Object map with {object_map.shape[2]} channels; Extracting the first one."
            )
            object_map = object_map[..., 0]
        space_dim = object_map.ndim
        skl_validity_mode = "multi"
    else:
        obj_map_path = path_t(__file__).parent / (data + ".npz")
        if obj_map_path.is_file():
            object_map = nmpy.load(str(obj_map_path))
            try:
                object_map = object_map["obj_map"]
            except KeyError:
                object_map = object_map["object_map"]
            space_dim = object_map.ndim
            skl_validity_mode = "multi"
        else:
            raise FileNotFoundError(obj_map_path)

    return space_dim, object_map, skl_validity_mode


def _PlotVariousMaps(
    object_map: array_t,
    rebuilt_obj_map: array_t,
    skl_map: array_t,
    rebuilt_skl_map: array_t,
    rebuilt_skl_mww: array_t,
    /,
) -> None:
    """"""
    images = (
        object_map,
        skl_map,
        rebuilt_skl_map,
        rebuilt_skl_mww,
        rebuilt_skl_map + object_map,
        rebuilt_skl_map + rebuilt_obj_map,
    )
    colormaps = (
        COLORMAP_FOR_IMG,
        COLORMAP_FOR_IMG,
        COLORMAP_FOR_IMG,
        COLORMAP_FOR_IMG,
        COLORMAP_FOR_IMG,
        COLORMAP_FOR_IMG,
    )
    titles = (
        "Object",
        "Skl",
        "Rebuilt Skl",
        "Rebuilt Skl WW",
        "Rebuilt Skl Over Obj",
        "Rebuilt Skl Over Rebuilt Obj",
    )
    _, axes = pypl.subplots(2, 3)
    _PrepareImagePlots(axes, images, colormaps, titles)

    images = (object_map, rebuilt_obj_map, object_map - rebuilt_obj_map)
    colormaps = (COLORMAP_FOR_IMG, COLORMAP_FOR_IMG, COLORMAP_FOR_DIFF)
    titles = ("Object", "Rebuilt", "Error")
    _, axes = pypl.subplots(ncols=images.__len__())
    _PrepareImagePlots(axes, images, colormaps, titles)


def _PlotSkeletons(
    skl_graph: skl_graph_t,
    object_map: array_t,
    back_to_networkx: ntkx.MultiGraph,
    space_dim: int,
    /,
) -> None:
    """"""
    if (space_dim == 3) or (skl_graph.n_nodes > 300):
        labels = {"label": False}
        edge_labels = None
    else:
        labels = {"label": (True, 6.0, "k")}
        edge_labels = EdgeLabelsForPlot(skl_graph)
    directions = {"direction": skl_graph.n_nodes <= 50}
    skl_graph.SetPlotStyles(node={1: "m", 3: "y"}, **directions, **labels)

    if space_dim == 2:
        axes = Plot(
            skl_graph,
            mode=plot_mode_e.Networkx,
            should_block=False,
            should_return_axes=True,
        )
        axes.set_title("Networkx Mode")

    axes = Plot(
        skl_graph,
        mode=plot_mode_e.SKL_Curve,
        should_block=False,
        should_return_axes=True,
    )
    axes.set_title("Curve Mode")

    axes = Plot(skl_graph, should_block=False, should_return_axes=True)
    axes.set_title("Pixel Mode")

    if space_dim == 2:
        _, axes = pypl.subplots()
        axes.matshow(object_map)
        Plot(skl_graph, axes=axes, should_block=False)
        axes.set_title("Pixel Mode over Object Map")

        for node_positions in (None, NodePositionsForPlot(skl_graph)):
            axes = PlotNetworkXGraph(
                back_to_networkx,
                NodePositions=node_positions,
                node_colors=NodeColors(skl_graph),
                with_node_labels=skl_graph.label_styles[0].show,
                node_font_size=int(round(skl_graph.label_styles[0].size)),
                edge_width=skl_graph.edge_styles[0].size,
                edge_labels=edge_labels,
                edge_font_size=int(round(skl_graph.label_styles[1].size)),
                should_block=False,
                should_return_axes=True,
            )
            if node_positions is None:
                axes.set_title("Back to NetworkX with auto-layout")
            else:
                axes.set_title("Back to NetworkX with node positions")


def _PlotSKNWSkeleton(
    sknw_graph: ntkx.Graph | ntkx.MultiGraph | None,
    object_map: array_t,
    space_dim: int,
    /,
) -> None:
    """"""
    if (sknw_graph is not None) and (space_dim == 2):
        # From: https://github.com/Image-Py/sknw (previously: https://github.com/yxdragon/sknw)
        sknw.draw_graph(object_map, sknw_graph)
        pypl.title("SKNW")
        pypl.tight_layout(pad=0.3)


def _PrepareImagePlots(all_axes, images, colormaps, titles, /) -> None:
    """"""
    for axes, img, cmap, title in zip(all_axes.flat, images, colormaps, titles):
        axes.matshow(img, cmap=cmap)
        axes.set_title(title)
        axes.set_axis_off()
    pypl.tight_layout(pad=0.3)


def Main(data, just_test, /) -> None:
    """"""
    # --- Object map
    space_dim, object_map, skl_validity_mode = _ObjectMap(data)
    nmpy.savez_compressed("/tmp/object_map", object_map=object_map)

    # --- Skeleton maps
    skl_map, skl_width_map = SKLMapFromObjectMap(object_map, with_width=True)
    CheckSkeletonMap(skl_map, mode=skl_validity_mode)

    # --- Skeleton graphs
    skl_graph = skl_graph_t.NewFromSKLMap(skl_map, width_map=skl_width_map)
    binary_correctness, topology_correctness = Correctness(skl_graph, skl_map)
    print(
        f"Rebuilt skeleton = Binary? Topological? "
        f"{binary_correctness} {topology_correctness}"
    )

    back_to_networkx = skl_graph.AsNetworkX()

    if sknw is None:
        sknw_graph = None
    else:
        sknw_graph = sknw.build_sknw(skl_map)

    # --- Some info about skeleton graphs
    print(
        f"Obj map area={nmpy.count_nonzero(object_map)}\n\n"
        f"{skl_graph}\n"
        f"{skl_graph.Properties(prefix='    ')}"
    )
    invalidities = Invalidities(skl_graph)
    if invalidities.__len__() > 0:
        print("Invalidities:")
        print("\n".join(invalidities))
    if sknw_graph is not None:
        print(dir(sknw_graph))

    # --- No display
    if just_test:
        return

    # --- Various rebuilt maps
    object_map = object_map.astype(nmpy.uint8)
    rebuilt_skl_map = skl_graph.RebuiltSkeletonMap()
    rebuilt_skl_mww = skl_graph.RebuiltSkeletonMap(with_width=True)
    rebuilt_obj_map = skl_graph.RebuiltObjectMap()

    if space_dim == 3:
        object_map = nmpy.amax(object_map, axis=2)
        skl_map = nmpy.amax(skl_map, axis=2)
        rebuilt_skl_map = nmpy.amax(rebuilt_skl_map, axis=2)
        rebuilt_skl_mww = nmpy.amax(rebuilt_skl_mww, axis=2)
        rebuilt_obj_map = nmpy.amax(rebuilt_obj_map, axis=2)

    # --- Various plots
    _PlotVariousMaps(
        object_map, rebuilt_obj_map, skl_map, rebuilt_skl_map, rebuilt_skl_mww
    )
    _PlotSkeletons(skl_graph, object_map, back_to_networkx, space_dim)
    _PlotSKNWSkeleton(sknw_graph, object_map, space_dim)
    pypl.show()


if __name__ == "__main__":
    #
    import sys as sstm

    if sstm.argv.__len__() == 1:
        print("Call with arguments: horse|2d|3d [noplot]")
        sstm.exit(0)
    test_and_exit = (sstm.argv.__len__() > 2) and (sstm.argv[2] == "noplot")

    Main(sstm.argv[1], test_and_exit)
