# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import matplotlib.pyplot as pypl
import numpy as nmpy
import skimage.morphology as skmp
from special_cases import map_names, skl_maps

from skl_graph.map import SKLMapFromThickVersion


array_t = nmpy.ndarray


# fmt: off
_cross_3d_middle = (
    (0, 1, 0),
    (1, 0, 1),
    (0, 1, 0)
)
_cross_3d_side = (
    (0, 0, 0),
    (0, 1, 0),
    (0, 0, 0)
)
cross_3d = nmpy.dstack((_cross_3d_side, _cross_3d_middle, _cross_3d_side)).astype(nmpy.bool_)
# fmt: on
skl_maps += (cross_3d,)
map_names += ("3D Cross",)


def Exploded(array: array_t, /) -> array_t:
    """"""
    size = nmpy.array(array.shape) * 2
    output = nmpy.zeros(size - 1, dtype=array.dtype)
    output[::2, ::2, ::2] = array

    return output


for map_name, skl_map in zip(map_names, skl_maps):
    padded = nmpy.pad(skl_map, 1)
    filled = skmp.remove_small_holes(padded, area_threshold=5)
    thinned = SKLMapFromThickVersion(filled)

    print(
        f"SKL map validity: "
        f"original={SKLMapFromThickVersion(padded, should_only_check=True)}, "
        f"filled={SKLMapFromThickVersion(filled, should_only_check=True)}, "
        f"thinned={SKLMapFromThickVersion(thinned, should_only_check=True)}, "
        f"thinned==filled={nmpy.array_equal(thinned, filled)}",
    )

    if padded.ndim == 2:
        _, axess = pypl.subplots(ncols=3)
    else:
        figure = pypl.figure()
        axess = tuple(
            figure.add_subplot(1, 3, _idx, projection="3d") for _idx in range(1, 4)
        )
    for axes, map_, title in zip(
        axess, (padded, filled, thinned), ("SKL Map", "Filled", "Thin")
    ):
        if map_.ndim == 2:
            axes.matshow(map_)
            axes.set_title(title)
        else:
            exploded = Exploded(map_)
            x, y, z = nmpy.indices(nmpy.array(exploded.shape) + 1).astype(float) // 2
            x[0::2, :, :] += 0.2
            y[:, 0::2, :] += 0.2
            z[:, :, 0::2] += 0.2
            x[1::2, :, :] += 0.8
            y[:, 1::2, :] += 0.8
            z[:, :, 1::2] += 0.8
            axes.voxels(x, y, z, exploded)
            axes.set_title(f"{title} (volume={nmpy.sum(map_).item()})")

    pypl.show()
