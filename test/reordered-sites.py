# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import im_tools_36.shape as im36
import matplotlib.pyplot as pypl
import numpy as nmpy
import scipy.spatial.distance as dstc
import skimage.data as sidt
import skimage.util as sitl

from skl_graph.map import SKLMapFromObjectMap, CheckSkeletonMap
from skl_graph.graph import skl_graph_t


array_t = nmpy.ndarray


def ReOrderedSitesWODistances(sites: tuple[array_t, ...],/) -> tuple[array_t, ...]:
    """
    If the number of sites is 1 or 2, the input argument is returned (i.e., no copy is made).

    Parameters
    ----------
    sites

    Returns
    -------

    """
    n_sites = sites[0].size
    if n_sites < 3:
        return sites

    dim = sites.__len__()
    sites_as_array = nmpy.transpose(nmpy.array(sites))
    reordered_coords = [nmpy.array([sites[idx][0] for idx in range(sites.__len__())])]
    unvisited_slc = nmpy.ones(n_sites, dtype=nmpy.bool_)
    unvisited_slc[0] = False
    unvisited_sites = None
    end_point = None
    pre_done = False
    post_done = False

    while unvisited_slc.any():
        if post_done:
            neighbor_idc = ()
        else:
            end_point = reordered_coords[-1]
            neighbor_idc, unvisited_sites = _NeighborIndices(
                dim, sites_as_array, unvisited_slc, end_point
            )

        if (neighbor_idc.__len__() == 1) or post_done:
            also_grow_first = (reordered_coords.__len__() > 1) and not pre_done
            if not post_done:
                c_idx = neighbor_idc[0]
                reordered_coords.append(unvisited_sites[c_idx, :])
                unvisited_slc[nmpy.where(unvisited_slc)[0][c_idx]] = False
            if also_grow_first:
                end_point = reordered_coords[0]
                neighbor_idc, unvisited_sites = _NeighborIndices(
                    dim, sites_as_array, unvisited_slc, end_point
                )
                if neighbor_idc.__len__() == 1:
                    c_idx = neighbor_idc[0]
                    reordered_coords = [unvisited_sites[c_idx, :]] + reordered_coords
                    unvisited_slc[nmpy.where(unvisited_slc)[0][c_idx]] = False
                elif neighbor_idc.__len__() == 0:
                    pre_done = True  # End point has been reached
                else:
                    raise RuntimeError(
                        f"{neighbor_idc.__len__()} neighbors when only 1 is expected\n"
                        f"{sites}\n{reordered_coords}\n{unvisited_slc}\n{end_point}"
                    )
        elif neighbor_idc.__len__() == 2:
            if reordered_coords.__len__() == 1:
                idx1, idx2 = neighbor_idc
                reordered_coords = [unvisited_sites[idx1, :]] + reordered_coords
                reordered_coords.append(unvisited_sites[idx2, :])
                true_map = nmpy.where(unvisited_slc)[0]
                unvisited_slc[true_map[idx1]] = False
                unvisited_slc[true_map[idx2]] = False
            else:
                raise RuntimeError(
                    f"2 neighbors when only 1 is expected\n"
                    f"{sites}\n{reordered_coords}\n{unvisited_slc}\n{end_point}"
                )
        elif neighbor_idc.__len__() == 0:
            post_done = True  # End point has been reached
        else:
            raise RuntimeError(
                f"{neighbor_idc.__len__()} neighbors when only 1 or 2 are expected\n"
                f"{sites}\n{reordered_coords}\n{unvisited_slc}\n{end_point}"
            )

    reordered_coords = nmpy.array(reordered_coords)
    reordered_coords = tuple(reordered_coords[:, idx_] for idx_ in range(dim))

    return reordered_coords


def _NeighborIndices(
    dim: int, sites: array_t, unvisited_slc: array_t, end_point: array_t,/
) -> tuple[array_t, array_t]:
    """"""
    unvisited_sites = sites[unvisited_slc, :]

    distances = nmpy.fabs(unvisited_sites - nmpy.reshape(end_point, (1, dim)))
    neighbor_idc = nmpy.nonzero(nmpy.all(distances <= 1, axis=1))[0]

    return neighbor_idc, unvisited_sites


def ReOrderedSitesWPairwiseDistances(sites: tuple[array_t, ...],/) -> tuple[array_t, ...]:
    """"""
    n_sites = sites[0].size

    if n_sites > 2:
        sites_as_array = nmpy.transpose(nmpy.array(sites))
        pairwise_dists = dstc.squareform(dstc.pdist(sites_as_array, "chebyshev"))
        reordered_sites_nfo = [(0, sites_as_array[0, :])]
        visited_sites = {0}

        while visited_sites.__len__() < n_sites:
            s_idx, first_site = reordered_sites_nfo[0]
            neighbor_idc = list(
                set((pairwise_dists[s_idx, :] == 1).nonzero()[0]) - visited_sites
            )
            # Length is equal to zero when reaching an extremity
            if neighbor_idc.__len__() > 0:
                reordered_sites_nfo.insert(
                    0, (neighbor_idc[0], sites_as_array[neighbor_idc[0], :])
                )
                visited_sites.add(neighbor_idc[0])

            if neighbor_idc.__len__() == 2:
                # The one seed + the one just added above = 2
                assert reordered_sites_nfo.__len__() == 2
                neighbor_idc[0] = neighbor_idc[1]
            else:
                s_idx, last_point = reordered_sites_nfo[-1]
                neighbor_idc = tuple(
                    set((pairwise_dists[s_idx, :] == 1).nonzero()[0]) - visited_sites
                )
                # Length is equal to zero when reaching an extremity
                if neighbor_idc.__len__() == 0:
                    continue

            reordered_sites_nfo.append(
                (neighbor_idc[0], sites_as_array[neighbor_idc[0], :])
            )
            visited_sites.add(neighbor_idc[0])

        reordered_coords = nmpy.array(
            tuple(site_nfo[1] for site_nfo in reordered_sites_nfo)
        )
        reordered_coords = tuple(
            reordered_coords[:, idx_] for idx_ in range(sites.__len__())
        )
    #
    else:
        reordered_coords = sites

    return reordered_coords


def Main() -> None:
    """"""
    object_map = sitl.invert(sidt.horse())

    skl_map, skl_width_map = SKLMapFromObjectMap(object_map, with_width=True)
    CheckSkeletonMap(skl_map)
    skl_graph = skl_graph_t.NewFromSKLMap(skl_map, width_map=skl_width_map)

    edge_map = nmpy.empty_like(object_map)
    for e_idx, (source, target, details) in enumerate(
        skl_graph.edges.data("details"), start=1
    ):
        print(f"EDGE: {e_idx}")
        edge_map.fill(0)
        edge_map[details.sites] = 1

        sites = nmpy.nonzero(edge_map)
        reorder_1 = ReOrderedSitesWODistances(sites)
        reorder_2 = ReOrderedSitesWPairwiseDistances(sites)

        equalities_12 = sites.__len__() * [True]
        reversed_eqs_12 = sites.__len__() * [True]
        for c_idx, (coords_1, coords_2) in enumerate(zip(reorder_1, reorder_2)):
            equalities_12[c_idx] = nmpy.array_equal(coords_1, coords_2)
            reversed_eqs_12[c_idx] = nmpy.array_equal(
                coords_1, tuple(reversed(coords_2))
            )

        for other, equ, req, reordered in zip(
            (2,),
            (equalities_12,),
            (reversed_eqs_12,),
            (reorder_2,),
        ):
            if not (all(equ) or all(req)):
                print(f"    1 vs {other}: {details.uid}: {sites[0].__len__()}")
                if sites[0].__len__() < 20:
                    print(
                        f"        {sites} ->\n        {reorder_1}\n        {reordered}"
                    )
                img = nmpy.zeros(object_map.shape, dtype=nmpy.uint64)
                for s_idx in range(reordered[0].size):
                    img[reordered[0][s_idx], reordered[1][s_idx]] = s_idx + 1
                pypl.matshow(im36.AutoCropped(img))
                pypl.show()


if __name__ == "__main__":
    #
    Main()
