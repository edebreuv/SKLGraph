# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import matplotlib.pyplot as pypl
import numpy as nmpy
import skimage.io as siio

from skl_graph.map import SKLMapFromObjectMap, CheckSkeletonMap
from skl_graph.graph import skl_graph_t
from skl_graph.task.check.graph import Invalidities
from skl_graph.task.plot.graph import Plot
from skl_graph.type.plot import plot_mode_e


img = siio.imread("rcc.png")
skl_map, skl_width_map = SKLMapFromObjectMap(img, with_width=True)
CheckSkeletonMap(skl_map, mode="multi")
skl_graph = skl_graph_t.NewFromSKLMap(skl_map, width_map=skl_width_map)

print(
    f"Obj map area={nmpy.count_nonzero(skl_map)}\n\n"
    f"{skl_graph}\n"
    f"{skl_graph.Properties(prefix='    ')}"
)
invalidities = Invalidities(skl_graph)
if invalidities.__len__() > 0:
    print("Invalidities:")
    print("\n".join(invalidities))

Plot(skl_graph, mode=plot_mode_e.Networkx, should_block=False)
pypl.matshow(img, cmap="gray")

pypl.show()
