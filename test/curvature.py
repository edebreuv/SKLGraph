# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import matplotlib.pyplot as pypl
import numpy as nmpy
from skimage.draw import ellipse_perimeter as EllipsePerimeter

from skl_graph.graph import skl_graph_t
from skl_graph.map import SKLMapFromThickVersion
from skl_graph.task.plot.graph import Plot, plot_mode_e


IMAGE_SIZE = 201


skl_map = nmpy.empty((IMAGE_SIZE, IMAGE_SIZE), dtype=nmpy.bool_)

for row_radius in (40, 60):
    for col_radius in (60, 80):
        skl_map.fill(False)
        ellipse = EllipsePerimeter(
            IMAGE_SIZE // 2 + 5, IMAGE_SIZE // 2 + 10, row_radius, col_radius
        )
        skl_map[ellipse] = True
        SKLMapFromThickVersion(skl_map, in_place=True)

        skl_graph = skl_graph_t.NewFromSKLMap(skl_map)

        _, axess = pypl.subplots(ncols=3)
        # axess = (*axess[0], *axess[1])
        axess[0].matshow(skl_map)
        axess[0].set_title(f"row={row_radius}, col={col_radius}")
        Plot(
            skl_graph,
            mode=plot_mode_e.SKL_Curve,
            axes=axess[1],
            should_block=False,
        )
        for *_, details in skl_graph.edges.data("details"):
            sites, arc_lengths, directions = details.Directions()
            changes = details.DirectionChanges()

            axess[1].quiver(
                sites[1],
                IMAGE_SIZE - 1 - sites[0],
                directions[1],
                -directions[0],
                color="b",
                linewidth=1,
            )

            # curvatures = nmpy.array(details.Curvatures())
            # summary = nmpy.median(curvatures).item()
            # summary_non_null = nmpy.median(curvatures[curvatures != 0]).item()
            # axess[2].plot(curvatures)
            # axess[2].set_title(
            #     f"Median = {summary}\n{summary_non_null}\nSum = {nmpy.sum(curvatures)}"
            # )

            # radii = nmpy.zeros_like(curvatures)
            # safe_radii = curvatures != 0.0
            # radii[safe_radii] = 1.0 / curvatures[safe_radii]
            # summary = nmpy.median(radii).item()
            # summary_non_null = nmpy.median(radii[radii != 0]).item()
            # axess[3].plot(radii)
            # axess[3].set_title(
            #     f"Median = {round(summary, 1)}\n{round(summary_non_null, 1)}"
            # )

            axess[2].plot(changes)
            axess[2].set_title(
                f"Avg. = {round(nmpy.mean(changes).item(), 3)}\n"
                f"{round(nmpy.mean(changes[changes > 0.0]).item(), 3)}\n"
                f"+-{round(nmpy.sum(changes).item(), 3)}, "
                f"+{round(nmpy.sum(changes[changes > 0.0]).item(), 3)}"
            )

        pypl.show()
